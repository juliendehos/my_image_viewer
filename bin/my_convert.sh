#!/bin/sh

if [ $# -lt 1 ] ; then
    echo "usage: $0 <image1> ..."
    exit
fi

CONVERT=`which convert 2> /dev/null`

if ! [ -f "${CONVERT}" ] ; then
    echo "Error: convert not found. Please install ImageMagick."
    exit 1
fi

for n in $@; do
    IMG=$n
    PNG="${IMG%.*}.png"
    ${CONVERT} $IMG $PNG
    echo "$IMG -> $PNG"
done

