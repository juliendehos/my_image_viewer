#!/bin/sh

if [ $# -lt 1 ] ; then
    echo "usage: $0 <image1> ..."
    exit
fi

EXRTOPNG=`which exrtopng 2> /dev/null`

if ! [ -f "${EXRTOPNG}" ] ; then
    echo "Error: exrtopng not found. Please install openexr viewer."
    exit 1
fi

for n in $@; do
    IMG=$n
    PNG="${IMG%.*}.png"
    ${EXRTOPNG} $IMG $PNG
    echo "$IMG -> $PNG"
done

