with import <nixpkgs> {}; 

let
  myopencv3 = opencv3.override { enableGtk2 = true; };
in

stdenv.mkDerivation {
    name = "my_image_viewer";
    buildInputs = [ stdenv pkgconfig cmake myopencv3 boost imagemagick exrtools ];
    src = ./.;
}

