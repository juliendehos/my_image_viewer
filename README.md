# my_image_viewer
[![Build status](https://gitlab.com/juliendehos/my_image_viewer/badges/master/build.svg)](https://gitlab.com/juliendehos/my_image_viewer/pipelines) 

```
mkdir build
cd build
cmake ..
make
sudo make install
my_image_viewer ../data/macbeth_jour/*.jpg
```

![](screenshot.png)

