#include "Utils.hpp"

#include <iostream>

using namespace std;

void printInfoColorSpaces() 
{
    cout << "rgb : 0 < r < 1; 0 < g < 1; 0 < b < 1" << endl;
    cout << "lab : 0 < l < 100; -127 < a < 127; -127 < b < 127" << endl;
    cout << "luv : 0 < l < 100; -134 < u < 220; -134 < v < 220" << endl;
    cout << "hsv : 0 < h < 360; 0 < s < 1; 0 < v < 1" << endl;
}

cv::Mat loadHdrQuiet(const string & filename) 
{
    // open image
    cv::Mat imageFromFile = cv::imread(filename, 
            cv::IMREAD_ANYDEPTH | cv::IMREAD_ANYCOLOR);
    if (not imageFromFile.data) 
    {
        cout << "error: unable to open " << filename << endl;
        exit(-1);
    }

    // convert to CV_32F and normalize
    cv::Mat imageToAnalyze; 
    switch (imageFromFile.depth()) 
    {
        case CV_8U:
            imageFromFile.convertTo(imageToAnalyze, CV_32F);
            imageToAnalyze *= 1.0/255.0;
            break;
        case CV_32F:
            cv::normalize(imageFromFile, imageToAnalyze, 0, 1, 
                    cv::NORM_MINMAX, CV_32F);
            break;
        default:
            cout << "image format not supported" << endl;
            exit(-1);
    }

    return imageToAnalyze;
}

