#include "Utils.hpp"

#include <iomanip>
#include <sstream>
#include <string>

using namespace cv;

int g_sizeRegion = 8;
int g_xRegion = 16;
int g_yRegion = 16;
int g_nbImages;
int g_imageIndex; // in [1, g_nbImages]
char * g_imageName;

Mat g_imageToAnalyze;

void onMouse(int event, int x, int y, int, void *)
{
    if (event == EVENT_LBUTTONDOWN)
    {
        g_xRegion = x;
        g_yRegion = y;

        // display BGR and BGRbar values in stdout
        int w = g_imageToAnalyze.cols;
        int h = g_imageToAnalyze.rows;
        int xRegion = std::min(w-g_sizeRegion, std::max(0, g_xRegion));
        int yRegion = std::min(h-g_sizeRegion, std::max(0, g_yRegion));
        Rect rectRegion(xRegion, yRegion, g_sizeRegion, g_sizeRegion);
        Mat matRegionBGR = g_imageToAnalyze(rectRegion);
        Scalar couleurRegionBGR = mean(matRegionBGR);
        Scalar somme = sum(couleurRegionBGR);
        Scalar couleurRegionBGRbar;
        divide(couleurRegionBGR, Scalar::all(somme[0]), couleurRegionBGRbar);
        std::cout << couleurRegionBGR[0] << ";" 
            << couleurRegionBGR[1] << ";" 
            << couleurRegionBGR[2] << ";"
            << couleurRegionBGRbar[0] << ";" 
            << couleurRegionBGRbar[1] << ";" 
            << couleurRegionBGRbar[2] << std::endl;
    }
}

void initImage(const char * filename, Mat & img, int & w, int & h)
{
    // charge l'image
    Mat imgSrc = loadHdrQuiet(filename);
    w = imgSrc.cols;
    h = imgSrc.rows;

    // agrandit la zone d'affichage pour le texte
    img = Mat::zeros(h+120, std::max(w,600), imgSrc.type());
    imgSrc.copyTo(img(Rect(0, 0, w, h)));
}

int main(int argc, char ** argv)
{
    // paramètres de la ligne de commande
    if (argc < 2) 
    {
        std::cout << "usage: " << argv[0] << " <images...> " << std::endl;
        exit(-1);
    }

    g_imageIndex = 1;
    g_nbImages = argc - 1;
    g_imageName = argv[g_imageIndex];

    int w, h;
    initImage(g_imageName, g_imageToAnalyze, w, h);

    // initialise l'affichage
    namedWindow("Display window", WINDOW_AUTOSIZE);
    setMouseCallback("Display window", onMouse, 0);
    printInfoColorSpaces();
    Scalar couleurDessin = Scalar(0,1,0);
    Scalar couleurTextBlanc = Scalar(1,1,1);
    Scalar couleurTextOk = Scalar(0.2,1,0.2);
    Scalar couleurTextCold = Scalar(1,0.5,0.5);
    Scalar couleurTextHot = Scalar(0.2,0.2,1);

    // boucle principale
    while (true)
    {

        //////////////////////////////////////////////////
        // calcule les régions
        //////////////////////////////////////////////////

        // sclère
        int xRegion = std::min(w-g_sizeRegion, std::max(0, g_xRegion));
        int yRegion = std::min(h-g_sizeRegion, std::max(0, g_yRegion));
        Rect rectRegion(xRegion, yRegion, g_sizeRegion, g_sizeRegion);

        // bgr
        Mat matRegionBGR = g_imageToAnalyze(rectRegion);
        Scalar couleurRegionBGR = mean(matRegionBGR);
        Scalar somme = sum(couleurRegionBGR);
        Scalar couleurRegionBGRbar;
        divide(couleurRegionBGR, Scalar::all(somme[0]), couleurRegionBGRbar);
        double couleurAvgBgr = somme[0] / 3.0;

        // luv
        Mat matRegionLUV;
        cvtColor(matRegionBGR, matRegionLUV, COLOR_BGR2Luv);
        Scalar couleurRegionLUV = mean(matRegionLUV);

        // lab
        Mat matRegionLAB;
        cvtColor(matRegionBGR, matRegionLAB, COLOR_BGR2Lab);
        Scalar couleurRegionLAB = mean(matRegionLAB);

        // hsv
        Mat matRegionHSV;
        cvtColor(matRegionBGR, matRegionHSV, COLOR_BGR2HSV);
        Scalar couleurRegionHSV = mean(matRegionHSV);

        //////////////////////////////////////////////////
        // affichage
        //////////////////////////////////////////////////

        // reinitialise l'image affichée
        Mat imageToShow = g_imageToAnalyze.clone();
        Scalar couleurText;
        if (couleurAvgBgr < 0.3) couleurText = couleurTextCold;
        else if (couleurAvgBgr > 0.7) couleurText = couleurTextHot;
        else couleurText = couleurTextOk;

        // affiche les commandes disponibles
        std::ostringstream oss;
        oss << "commands : left click, +/-, page up/down, up/down/left/right, esc";
        putText(imageToShow, oss.str(), Point(10, h+20), FONT_HERSHEY_SIMPLEX, 0.4, Scalar::all(255), 1);

        // couleur
        oss.str(std::string());
        oss << "bgr=" << std::setprecision(3) << couleurRegionBGR << " bgrBar=" << couleurRegionBGRbar << " avg=" << couleurAvgBgr;
        putText(imageToShow, oss.str(), Point(10, h+40), FONT_HERSHEY_SIMPLEX, 0.4, couleurText, 1);
        oss.str(std::string());
        oss << "lab=" << couleurRegionLAB << " luv=" << couleurRegionLUV << " hsv=" << couleurRegionHSV;
        putText(imageToShow, oss.str(), Point(10, h+60), FONT_HERSHEY_SIMPLEX, 0.4, couleurText, 1);
        rectangle(imageToShow, rectRegion, couleurDessin, 1);

        // nom de l'image
        oss.str(std::string());
        oss << g_imageIndex << " / " << g_nbImages << " -> " << g_imageName;
        putText(imageToShow, oss.str(), Point(10, h+80), FONT_HERSHEY_SIMPLEX, 0.4, couleurTextBlanc, 1);

        // zone de selection
        oss.str(std::string());
        oss << "x=" << g_xRegion << " y=" << g_yRegion << " size=" << g_sizeRegion;
        putText(imageToShow, oss.str(), Point(10, h+100), FONT_HERSHEY_SIMPLEX, 0.4, couleurTextBlanc, 1);

        imshow( "Display window", imageToShow );

        //////////////////////////////////////////////////
        // gestion des evenements utilisateurs
        //////////////////////////////////////////////////

        int k = cv::waitKey(100) % 0x100;
        if (k == 27)        // esc
            break;
        else if (k == 119)  // -
            g_sizeRegion = std::max(4, g_sizeRegion-4);
        else if (k == 226)  // +
            g_sizeRegion = g_sizeRegion+4;
        else if (k == 82)  // up
            g_yRegion--;
        else if (k == 84)  // down
            g_yRegion++;
        else if (k == 81)  // left
            g_xRegion--;
        else if (k == 83)  // right
            g_xRegion++;
        else if (k == 86)  // page down
        {
            g_imageIndex = g_imageIndex == 1 ? g_nbImages : g_imageIndex-1;
            g_imageName = argv[g_imageIndex];
            initImage(g_imageName, g_imageToAnalyze, w, h);
        }
        else if (k == 85)  // page up
        {
            g_imageIndex = g_imageIndex == g_nbImages ? 1 : g_imageIndex+1;
            g_imageName = argv[g_imageIndex];
            initImage(g_imageName, g_imageToAnalyze, w, h);
        }
        //else if (k != -1)
        //{
        //    std::cout << "warning: unknown command (" << k << ")" << std::endl;
        //}
    }

    return 0;
}

