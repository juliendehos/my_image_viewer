#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <opencv2/opencv.hpp>
#include <string>

void printInfoColorSpaces();
cv::Mat loadHdrQuiet(const std::string & filename);

#endif

